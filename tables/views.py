from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound
from django.template import loader
import requests, json
# Create your views here.
def index(request):
    url = 'http://codename-codeniacs.herokuapp.com/api/v1/9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b/users/'

    params = dict(
        format='json'
    )

    resp = requests.get(url=url, params=params)
    data = json.loads(resp.text)
    template = loader.get_template("index.html")
    context = {
        'users': data
    }
    return HttpResponse(template.render(request=request, context=context))

def get_user(data, name):
    for user in data:
        if user['name'] == name:
            user['id'] = data.index(user) + 1
            return user

def user_page(request, name):
    url = 'http://codename-codeniacs.herokuapp.com/api/v1/9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b/users/'

    params = dict(
        format='json'
    )

    resp = requests.get(url=url, params=params)
    data = json.loads(resp.text)
    template = loader.get_template("page.html")
    context = {
        'user': get_user(data, name)
    }
    if context['user'] != None:
        return HttpResponse(template.render(request=request, context=context))
    else:
        return HttpResponseNotFound('<h1>Whoops!</h1><br><p>No such page <b>{}</b></p'.format(name))
